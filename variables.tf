variable "customer_tags" {
  default = {}
  description = "appends customer tags when defined from -var-file"
}

variable region {
  default = "us-east-1"
}

variable "demo_s3_bucket_name" {
}
