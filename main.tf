terraform {
  backend "local" {}
}

provider "aws" {
  version = "2.59.0"
  region = var.region
}

module "s3" {
  source = "../tf-library/s3"
  customer_tags = var.customer_tags
  demo_s3_bucket_name = var.demo_s3_bucket_name
}